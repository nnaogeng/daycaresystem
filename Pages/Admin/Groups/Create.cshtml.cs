using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Groups
{
    public class CreateModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public CreateModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public ChildrenGroup ChildrenGroup { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.ChildrenGroups.Add(ChildrenGroup);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
