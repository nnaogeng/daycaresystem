using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Groups
{
    public class DeleteModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public DeleteModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ChildrenGroup ChildrenGroup { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ChildrenGroup = await _context.ChildrenGroups.FirstOrDefaultAsync(m => m.Id == id);

            if (ChildrenGroup == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ChildrenGroup = await _context.ChildrenGroups.FindAsync(id);

            if (ChildrenGroup != null)
            {
                _context.ChildrenGroups.Remove(ChildrenGroup);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
