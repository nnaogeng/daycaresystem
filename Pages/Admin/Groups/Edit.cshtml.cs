using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Groups
{
    public class EditModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public EditModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public ChildrenGroup ChildrenGroup { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ChildrenGroup = await _context.ChildrenGroups.FirstOrDefaultAsync(m => m.Id == id);

            if (ChildrenGroup == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(ChildrenGroup).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChildrenGroupExists(ChildrenGroup.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ChildrenGroupExists(int id)
        {
            return _context.ChildrenGroups.Any(e => e.Id == id);
        }
    }
}
