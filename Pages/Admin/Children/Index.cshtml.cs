using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Children
{
    public class IndexModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public IndexModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Child> Child { get;set; }

        public const int RECORDS_PER_PAGE = 5;

        [BindProperty(SupportsGet =true)] 
        public int CurrentPage { get; set; } = 1;   
        public int TotalPages { get; set; } 
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;   


        public async Task OnGetAsync()
        {
            Child = await _context.Children.Skip((CurrentPage - 1) * RECORDS_PER_PAGE).Take(RECORDS_PER_PAGE).ToListAsync();            
            TotalPages = (int)Math.Ceiling(_context.Children.Count() * 1.0 / RECORDS_PER_PAGE);
        }
    }
}
