using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Children
{
    public class DeleteModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public DeleteModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Child Child { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Child = await _context.Children.FirstOrDefaultAsync(m => m.Id == id);

            if (Child == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Child = await _context.Children.FindAsync(id);

            if (Child != null)
            {
                _context.Children.Remove(Child);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
