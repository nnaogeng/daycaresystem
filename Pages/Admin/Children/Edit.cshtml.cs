using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Children
{
    public class EditModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;
        private IWebHostEnvironment _environment;

        public EditModel(DayCare.Data.ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        
        [BindProperty]
        public Child Child { get; set; }

        [BindProperty]
        [Display(Name = "Choose Photo File")]
        public IFormFile PhotoFile { get; set; }

        [BindProperty]
        [Display(Name = "Group")]
        public string GroupName { get; set; }

        [BindProperty]
        public IList<ChildrenGroup> GroupList { get;set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Child = await _context.Children.FirstOrDefaultAsync(m => m.Id == id);

            if (Child == null)
            {
                return NotFound();
            }
            
            GroupList = _context.ChildrenGroups.ToList();

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            if (PhotoFile != null) 
            {
                string[] names = PhotoFile.FileName.Split(".");
                string fileExtension = names[names.Length-1];
                string fileName = Child.FirstName + "_" + Child.LastName + "." + fileExtension;
                var file = Path.Combine(_environment.ContentRootPath, "wwwroot\\photos\\children", fileName); //Upload.FileName);
                using (var fileStream = new FileStream(file, FileMode.Create))
                {
                    await PhotoFile.CopyToAsync(fileStream);
                }            
                Child.PhotoFilePath = "/photos/children/" + fileName;
            }
          
            ChildrenGroup Group = await _context.ChildrenGroups.Where(g => String.Equals(g.GroupName, GroupName)).FirstOrDefaultAsync();
            Child.ChildrenGroup = Group;

            _context.Attach(Child).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ChildExists(Child.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ChildExists(int id)
        {
            return _context.Children.Any(e => e.Id == id);
        }
    }
}
