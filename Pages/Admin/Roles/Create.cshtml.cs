using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Roles
{
    public class CreateModel : PageModel
    {        
        private readonly RoleManager<IdentityRole> _roleManager;

        public CreateModel(RoleManager<IdentityRole> roleManager) {
            _roleManager = roleManager;
        }
        
        [BindProperty]
        [Required]
        public string RoleName { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            IdentityRole identityRole = new IdentityRole {
                Name = RoleName
            };                  
            IdentityResult resutl = await _roleManager.CreateAsync(identityRole);

            return RedirectToPage("./Index");
        }
    }
}
