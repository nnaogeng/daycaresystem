using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Roles
{
    public class DeleteModel : PageModel
    {
        private readonly RoleManager<IdentityRole> _roleManager;        

        public DeleteModel(RoleManager<IdentityRole> roleManager) {
            _roleManager = roleManager;
        }

        [BindProperty]
        public IdentityRole Role { get; set; }        

        public async Task<IActionResult> OnGetAsync(string id)
        {        
            Role = await _roleManager.FindByIdAsync(id);

            if (Role == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {                   
           
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var role = await _roleManager.FindByIdAsync(Role.Id);                
            IdentityResult result = await _roleManager.DeleteAsync(role);            
            if (!result.Succeeded) {
                return Page();
            } 
            return RedirectToPage("./Index");
                  
        }
    }
}
