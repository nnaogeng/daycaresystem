using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Roles
{
    public class EditModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;        

        public EditModel(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [BindProperty]
        public IdentityRole Role { get; set; }

        [BindProperty(SupportsGet =true)]
        public string id { get; set; }

        public async Task<IActionResult> OnGetAsync()
        {
        /*    if (id == null)
            {
                return NotFound();
            }
*/
            Role = await _roleManager.FindByIdAsync(id);

            if (Role == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            
            var role = await _roleManager.FindByIdAsync(Role.Id);
            role.Name = Role.Name;
            IdentityResult result = await _roleManager.UpdateAsync(role);            
            if (!result.Succeeded) {
               return Page();
           } 
           return RedirectToPage("./Index");
        }
    }
}
