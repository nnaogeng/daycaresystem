using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Users
{
    public class EditModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;        

        public EditModel(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [BindProperty(SupportsGet =true)]
        public ApplicationUser AppUser { get; set; }

        [BindProperty]
        public IList<IdentityRole> RoleList { get;set; }

        [BindProperty]
        public IList<string> RoleNames { get; set; }

        [BindProperty]
        public string Role { get; set; }

        [BindProperty(SupportsGet =true)]
        public string Id { get; set; }


        public async Task<IActionResult> OnGetAsync()
        {
            /*if (id == null)
            {
                return NotFound();
            }
*/
            RoleList = await _roleManager.Roles.ToListAsync();

           // AppUser = await _userManager.Users.FirstOrDefaultAsync(m => string.Equals(m.Id,id));
            AppUser = await _userManager.FindByIdAsync(Id);
            if (AppUser == null)
            {
                return NotFound();
            }
            
            RoleNames = await _userManager.GetRolesAsync(AppUser);
            if (RoleNames.Count != 0) {
                Role = RoleNames[0];
            }                    
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            ApplicationUser AppUserDb = await _userManager.FindByIdAsync(Id);
            if (AppUserDb == null)
            {
                return NotFound();
            }

            RoleNames = await _userManager.GetRolesAsync(AppUserDb);
            if (RoleNames.Count != 0) {
                await _userManager.RemoveFromRoleAsync(AppUserDb, RoleNames[0]);
            }
            //string Role = Request.Form["Role"];
            await _userManager.AddToRoleAsync(AppUserDb, Role);
            RoleNames = await _userManager.GetRolesAsync(AppUserDb);
            
            AppUserDb.PhoneNumber = AppUser.PhoneNumber;
            AppUserDb.FirstName = AppUser.FirstName;           
            AppUserDb.LastName = AppUser.LastName;
            await _userManager.UpdateAsync(AppUserDb);

            return RedirectToPage("./Index");
        }

        private bool UserExists(int id)
        {
            return _userManager.Users.Any(e => string.Equals(e.Id, id));
        }
    }
}
