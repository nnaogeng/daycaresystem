using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Users
{
    public class DeleteModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;        

        public DeleteModel(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        [BindProperty(SupportsGet =true)]
        public ApplicationUser AppUser { get; set; }

        [BindProperty]
        public IList<string> RoleNames { get; set; }

        [BindProperty]
        public string Role { get; set; }        

        public async Task<IActionResult> OnGetAsync(string id)
        {        
            AppUser = await _userManager.FindByIdAsync(id);
            if (AppUser == null)
            {
                return NotFound();
            }
            RoleNames = await _userManager.GetRolesAsync(AppUser);
            if (RoleNames.Count != 0) {
                Role = RoleNames[0];
            }  
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {        
            AppUser = await _userManager.FindByIdAsync(AppUser.Id);
            if (AppUser == null)
            {
                return NotFound();
            }
            RoleNames = await _userManager.GetRolesAsync(AppUser);
            if (RoleNames.Count != 0) {
                await _userManager.RemoveFromRoleAsync(AppUser, RoleNames[0]);
            }            

            await _userManager.DeleteAsync(AppUser);
            
            return RedirectToPage("./Index");
        }
    }
}
