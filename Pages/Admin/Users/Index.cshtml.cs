using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Users
{
    public class IndexModel : PageModel
    {   
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;        

        public IndexModel(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager) {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public IList<ApplicationUser> UserList { get;set; }
        public IList<string> RoleList { get;set; }

        public const int RECORDS_PER_PAGE = 5;

        [BindProperty(SupportsGet =true)] 
        public int CurrentPage { get; set; } = 1;   
        public int TotalPages { get; set; } 
        public bool ShowPrevious => CurrentPage > 1;
        public bool ShowNext => CurrentPage < TotalPages;   

        public async Task OnGetAsync()
        {
            UserList = await _userManager.Users.Skip((CurrentPage - 1) * RECORDS_PER_PAGE).Take(RECORDS_PER_PAGE).ToListAsync();
            //Child = await _context.Children.Skip((CurrentPage - 1) * RECORDS_PER_PAGE).Take(RECORDS_PER_PAGE).ToListAsync();            
            TotalPages = (int)Math.Ceiling(_userManager.Users.Count() * 1.0 / RECORDS_PER_PAGE);        
        }
    }
}
