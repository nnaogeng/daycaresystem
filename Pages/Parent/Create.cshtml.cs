using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.ComponentModel.DataAnnotations;
using System.IO;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Parent
{
    public class CreateModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly DayCare.Data.ApplicationDbContext _context;
        private IWebHostEnvironment _environment;

        public CreateModel(UserManager<ApplicationUser> userManager, DayCare.Data.ApplicationDbContext context, IWebHostEnvironment environment)
        {
            _userManager = userManager;
            _context = context;
            _environment = environment;
        }

        public ApplicationUser CurrentUser { get; set; }

        [BindProperty]
        public Child Child { get; set; }

        [BindProperty]
        [Display(Name = "Choose Photo File")]
        public IFormFile PhotoFile { get; set; }

        public IActionResult OnGet()
        {
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            Child.IsActive = true;
            if (!ModelState.IsValid)
            {
                return Page();
            }
            if (PhotoFile != null) 
            {
                string[] names = PhotoFile.FileName.Split(".");
                string fileExtension = names[names.Length-1];
                string fileName = Child.FirstName + "_" + Child.LastName + "." + fileExtension;
                var file = Path.Combine(_environment.ContentRootPath, "wwwroot\\photos\\children", fileName); //Upload.FileName);
                using (var fileStream = new FileStream(file, FileMode.Create))
                {
                    await PhotoFile.CopyToAsync(fileStream);
                }            
                Child.PhotoFilePath = "/photos/children/" + fileName;
            }
                
            CurrentUser = await _userManager.GetUserAsync(HttpContext.User);
            Child.Parent = CurrentUser;
            _context.Children.Add(Child);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
