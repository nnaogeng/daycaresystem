using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Parent
{
    public class IndexModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly DayCare.Data.ApplicationDbContext _context;

        public IndexModel(UserManager<ApplicationUser> userManager, DayCare.Data.ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public IList<Child> Child { get;set; }

        public ApplicationUser CurrentUser { get; set; }

        public async Task OnGetAsync()
        {
            CurrentUser = await _userManager.GetUserAsync(HttpContext.User);
            Child = await _context.Children.Where(child => string.Equals(child.Parent.Id, CurrentUser.Id)).ToListAsync();
        }
    }
}
