using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Parent
{
    public class ViewCommentModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public ViewCommentModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Child Child { get; set; }

        //public DailyComment Comment { get; set; }
        public IList<DailyComment> Comments { get; set; }

        [BindProperty]
        public string FormatedDate { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            FormatedDate = DateTime.Today.ToShortDateString();
            Child = await _context.Children.FirstOrDefaultAsync(m => m.Id == id);            
            Comments = await _context.DailyComments.Where(comment => comment.Child.Id == Child.Id && DateTime.Equals(comment.CommentDate.Date, DateTime.Today.Date)).ToListAsync();

            if (Child == null)
            {
                return NotFound();
            }
            if (Comments == null) {                
                Comments[0].Comment = "No comment today.";
            }
            return Page();
        }
    }
}
