using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Educator
{
    public class ChildrenListModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public ChildrenListModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }


        public IList<Child> Child { get;set; }

        public async Task OnGetAsync()
        {
            Child = await _context.Children.Include(child => child.ChildrenGroup).Where(child => child.ChildrenGroup.Id == Id && child.IsActive == true).ToListAsync();
        }
    }
}
