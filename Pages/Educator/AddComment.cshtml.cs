using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Educator
{
    public class AddCommentModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public AddCommentModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }        

        [BindProperty(SupportsGet =true)]
        public Child Child { get; set; }

        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }

        [BindProperty(SupportsGet =true)]
        public DailyComment Comment { get; set; }

        [BindProperty]
        public string FormatedCommentDate { get; set; }


        public IActionResult OnGet()
        {
            Child = _context.Children.FirstOrDefault(child => child.Id == Id); 
            //Comment.CommentDate = DateTime.Today;  
            FormatedCommentDate = DateTime.Today.ToShortDateString();
            return Page();
        }
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            Comment.Child = _context.Children.FirstOrDefault(child => child.Id == Id);
            Comment.CommentDate = DateTime.Today;
            _context.DailyComments.Add(Comment);
            await _context.SaveChangesAsync();

            return RedirectToPage("/Educator/Index");
        }
    }
}
