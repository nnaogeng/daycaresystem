using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Admin.Educator
{
    public class IndexModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public IndexModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<ChildrenGroup> ChildrenGroup { get;set; }

        public async Task OnGetAsync()
        {
            ChildrenGroup = await _context.ChildrenGroups.ToListAsync();
        }
    }
}
