﻿
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
namespace DayCare.Pages
{
    public class IndexModel : PageModel
    {
       private readonly IEmailSender _emailSender;

        public IndexModel(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public string EmailStatusMessage { get; set; }

        [Required(ErrorMessage = "Email can not be blank!")]
        [EmailAddress(ErrorMessage="A valid email address is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Name can not be blank!")]
        [Range(2,20)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Subject can not be blank!")]
        [Range(2,50)]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Message can not be blank!")]
        [Range(2,200)]
        public string Message { get; set; }


        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var email = Email;

            var subject = Name + "-" + Subject;

            var message = Message;

            await _emailSender.SendEmailAsync(email, subject, message);

            EmailStatusMessage = "Send test email was successful.";

            return Page();
        }
    }
}

