using System;
using System.Collections.Generic;
using System.Linq;
//using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;
//using Microsoft.AspNetCore.Mvc;

namespace DaycareSystem.Pages
{
    public class ContactusModel : PageModel
    {
        private readonly IEmailSender _emailSender;

        public ContactusModel(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public string EmailStatusMessage { get; set; }
        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel {
            

            [Required(ErrorMessage = "Email can not be blank!")]
            [EmailAddress(ErrorMessage="A valid email address is required")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Name can not be blank!")]
            
            public string Name { get; set; }

            [Required(ErrorMessage = "Subject can not be blank!")]
            
            public string Subject { get; set; }

            [Required(ErrorMessage = "Message can not be blank!")]
           
            public string Message { get; set; }
        }
        
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var email = Input.Email;

            var subject = Input.Name + "-" + Input.Subject ;

            var message = Input.Message;

            await _emailSender.SendEmailAsync(email, subject, message);

            EmailStatusMessage = "Send email was successful.";

            return Page();
        }
    }
}
