using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using DayCare.Data;
using DayCare.Models;

namespace DayCare.Pages.Test
{
    public class IndexModel : PageModel
    {
        private readonly DayCare.Data.ApplicationDbContext _context;

        public IndexModel(DayCare.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Child> Child { get;set; }

        public const int RECORDS_PER_PAGE = 2;
        [BindProperty(SupportsGet =true)] 
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; } 
        public bool ShowPrevious { get; set; }
        public bool ShowNext { get; set; }


        public void OnGet()
        {
            //Child = await _context.Children.ToListAsync(); 
            TotalPages = (int)Math.Ceiling(_context.Children.Count() * 1.0 / RECORDS_PER_PAGE);        
        }

        public PartialViewResult OnGetChildrenPagination()
        {              
            Child = _context.Children.Skip((CurrentPage - 1) * RECORDS_PER_PAGE).Take(RECORDS_PER_PAGE).ToList();
            TotalPages = (int)Math.Ceiling(_context.Children.Count() * 1.0 / RECORDS_PER_PAGE);
            ShowPrevious = CurrentPage > 1;
            ShowNext = CurrentPage < TotalPages;
            return Partial("_TestPartial", Child);
        }
    }
}
