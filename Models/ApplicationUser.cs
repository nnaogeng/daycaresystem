using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DayCare.Models
{
    public class ApplicationUser: IdentityUser
    {   
        [Display(Name = "First Name")]        
        [StringLength(30)]

        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [StringLength(30)]
        public string LastName { get; set; }    
    }
}