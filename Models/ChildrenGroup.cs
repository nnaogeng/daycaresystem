using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace DayCare.Models
{
    public class ChildrenGroup
    {
        public int Id { get; set; }

        [Display(Name = "Group Name")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string GroupName { get; set; }

        public ApplicationUser Educator { get; set; }
    }
}