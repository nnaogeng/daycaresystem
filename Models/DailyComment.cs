using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace DayCare.Models
{
    public class DailyComment
    {
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Comment { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime CommentDate { get; set; }
        
        public Child Child { get; set; }
    }
}