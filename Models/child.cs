using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace DayCare.Models
{
    public class Child
    {
        public int Id { get; set; }

        [Display(Name = "First Name")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string FirstName { get; set; }

        [Display(Name = "Last Name")]
        [Required]
        [StringLength(30, MinimumLength = 2)]
        public string LastName { get; set; }

        [Display(Name = "Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        [Display(Name = "Photo")]
        [StringLength(100)]
        public string PhotoFilePath { get; set; }

        [StringLength(100)]
        public string Allergery { get; set; }

        public bool IsActive { get; set; } 
        
        public ChildrenGroup ChildrenGroup { get; set; }

        public ApplicationUser Parent { get; set; }
         
    } 
}