using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using DayCare.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authorization;
using DayCare.Models;
using daycaresystem.Entities;
using daycaresystem.Service;
using Google.Cloud.AspNetCore.DataProtection.Kms;
using Google.Cloud.AspNetCore.DataProtection.Storage;
using Microsoft.AspNetCore.DataProtection;

namespace DayCare
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlite(
                    Configuration.GetConnectionString("DefaultConnection")));
            services.AddDefaultIdentity<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = false)
				.AddRoles<IdentityRole>()
                //.AddDefaultTokenProviders()
                //.AddDefaultUI()
                .AddEntityFrameworkStores<ApplicationDbContext>();
                
            services.AddRazorPages((options =>
            {
                options.Conventions.AuthorizeFolder("/Admin", "RoleIsAdmin");
                options.Conventions.AuthorizeFolder("/Educator", "RoleIsEducator");
                options.Conventions.AuthorizeFolder("/Parent", "RoleIsParent");
            }));

            //services.AddDataProtection().PersistKeysToGoogleCloudStorage(
                    //Configuration["DataProtection:Bucket"],
                    //Configuration["DataProtection:Object"])
                    
                //.ProtectKeysWithGoogleKms(
                    //Configuration["DataProtection:KmsKeyName"]);

            services.AddAuthorization(options =>
            {
                options.AddPolicy("RoleIsAdmin", policy =>
                    policy.RequireRole("Admin"));
                options.AddPolicy("RoleIsEducator", policy =>
                    policy.RequireRole("Admin", "Educator"));
                options.AddPolicy("RoleIsParent", policy =>
                policy.RequireRole("Admin", "Parent"));

                /*    policy.Requirements.Add(new AuthorizationPolicyBuilder()
                        .RequireAuthenticatedUser()
                        .RequireRole("Admin")
                        .Build())); */
            });
            services.Configure<EmailSettings>(Configuration.GetSection("EmailSettings"));
           
            services.AddAuthentication()
            .AddGoogle(options =>
            {
                options.ClientId = "816492516924-88ss3p6ob2htunli6gfv762afmrnc1hf.apps.googleusercontent.com";
                options.ClientSecret = "kjM-wU1BhqUtd--EeV8JK1tM";
            });

            services.AddAuthentication()
            .AddFacebook(facebookOptions =>
            {
                facebookOptions.AppId = "821681838365833";
                facebookOptions.AppSecret = "74703f4858898a1e22494b77c8d2d7f6";
            });
        
            services.Configure<IdentityOptions>(options =>
            {
                // Password settings.
                options.Password.RequireDigit = true;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = true;
                options.Password.RequireUppercase = true;
                options.Password.RequiredLength = 6;
                options.Password.RequiredUniqueChars = 1;

                // Lockout settings.
                options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                options.Lockout.MaxFailedAccessAttempts = 5;
                options.Lockout.AllowedForNewUsers = true;

                // User settings.
                options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                options.User.RequireUniqueEmail = false;
                
            });

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(5);

                options.LoginPath = "/Identity/Account/Login";
                options.AccessDeniedPath = "/Identity/Account/AccessDenied";
                options.SlidingExpiration = true;
            });


            services.AddSingleton<IEmailSender, EmailSender>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
            });
        }
    }
}
